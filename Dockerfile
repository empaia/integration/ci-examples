FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner@sha256:cfda70e8c0d3e87dfcf48ec8cce5282884f4760e2d2363d7cc255ce8e47ef05f AS builder
COPY . /dummy_service
WORKDIR /dummy_service
RUN python3 -m venv .venv
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base@sha256:8b9fc06bcbcaec02f0d09960d84159283d37e3814e17ab8b43357dceb31d423a
COPY --from=builder /dummy_service/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /dummy_service/dist /artifacts
RUN pip install /artifacts/*.whl
CMD ["uvicorn", "dummy_service.main:app", "--host", "0.0.0.0", "--port", "8000"]
