"""Test Main"""
from fastapi.testclient import TestClient

from dummy_service.main import app

client = TestClient(app)


def test_main():
    """Test Main"""
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}
