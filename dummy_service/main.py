"""Dummy Service"""
from fastapi import FastAPI

app = FastAPI()


@app.get("/alive")
def get_service_status():
    """Alive Function"""
    return {"status": "ok", "version": "0.1.0"}


@app.get("/")
def hello_world():
    """Hello World Function"""
    return {"msg": "Hello World"}
