# CI Examples

This repository contains a simple webservice and is intended to test the usage of [CI Templates](https://gitlab.com/empaia/integration/ci-templates).

In addition, the GitLab CI is to be used on all available runners. This is to ensure both the correct configuration of the runners and the applicability of the CI templates. The GitLab CI for different runners is defined in different branches (`runner-test-NAME`).

## Development

The following snippets can be run locally to fix at least some of the issues that the CI will check.

## Codecheck

```bash
poetry run black dummy_service
poetry run isort dummy_service
poetry run pycodestyle dummy_service
poetry run pylint dummy_service
```

## Container Scanning

```bash
docker build . -t registry.gitlab.com/empaia/integration/ci-examples
trivy --cache-dir .trivycache image registry.gitlab.com/empaia/integration/ci-examples
```

